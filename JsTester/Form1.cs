﻿using JsTester.Core;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Forms;
using JsTester.Constants;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;

namespace JsTester
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Main app directory
        /// </summary>
        string appDir;

        /// <summary>
        /// Wep panel reference
        /// </summary>
        WebPanel webPanel;

        /// <summary>
        /// Web logger
        /// </summary>
        WebLogger Logger;

        /// <summary>
        /// Syntax highlighter reference
        /// </summary>
        WebSyntax syntax = new WebSyntax();

        /// <summary>
        /// Html Tags processor reference
        /// </summary>
        HtmlTagsProcessor TagsProcessor;

        /// <summary>
        /// A default html content on startup
        /// </summary>
        private string DefaultHTML = string.Empty;
        public Form1()
        {
            InitializeComponent();
            ui_mode.SelectedIndex = 1;
            appDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            webPanel = new WebPanel();
            Logger = new WebLogger();
            TagsProcessor = new HtmlTagsProcessor();
        }

        /// <summary>
        /// Run a page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            webPanel.UpdatePage(ui_codebox,ui_browser);
        }

        private void ui_codebox_TextChanged(object sender, EventArgs e)
        {
            webPanel.UpdatePage(ui_codebox, ui_browser);
            //Logger.ClearConsole();
            syntax.CheckHTML(Logger, TagsProcessor);
            /*if (!string.IsNullOrEmpty(hasTags))
            {
                Logger.Warning($"Tags '{hasTags}' not found");
            }

            var notClosedTags = syntax.HasClosedTags();
            if (!string.IsNullOrEmpty(notClosedTags))
            {
                Logger.Warning($"Tags '{notClosedTags}' is not closed");
            }*/
        }

        private void ui_browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var length = System.Text.ASCIIEncoding.Unicode.GetByteCount(ui_codebox.Text);
            ui_title.Text = ui_browser.DocumentTitle;
            ui_status.Text = $"URL - {ui_browser.Url}; Status - {ui_browser.StatusText}; Length - {length} bytes;";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            syntax.Hightlight(ui_codebox);
            //ui_codebox.AutoCShow(2, TagsProcessor.HtmlTagsAutoC);
            Logger.Init();
            DefaultHTML = ui_codebox.Text;
        }

        private void ui_browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            //Logger.ClearConsole();// TODO web console loading
            ui_browser.Document.Window.Error -= new HtmlElementErrorEventHandler(Window_Error);
            ui_browser.Document.Window.Error += new HtmlElementErrorEventHandler(Window_Error);
        }


        private void Window_Error(object sender, HtmlElementErrorEventArgs e)
        {
            Logger.Error($"{e.Description} at {e.LineNumber} line");
                            
            e.Handled = true;
        }

        /// <summary>
        /// Mouse hover event for a GUI elements
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Gui_mouse_hover(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.Control)
            {
                ((System.Windows.Forms.Control)sender).BackColor = Color.DarkGray;
            }
            
        }

        /// <summary>
        /// Mouse leave event for a GUI elements
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Gui_mouse_leave(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.Control)
            {
                ((System.Windows.Forms.Control)sender).BackColor = Color.Transparent;
            }
        }

        /// <summary>
        /// Setup a settings form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            SettingsForm settingsForm = new SettingsForm();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            ui_codebox.Text = DefaultHTML;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsTester.Constants
{
    internal class WebConstants
    {
        /// <summary>
        /// Log console types
        /// </summary>
        public enum LogTypes
        {
            Error,
            Log,
            Warning
        }

        /// <summary>
        /// Web colors
        /// </summary>
        internal class Colors
        {
            public static string ErrorColor = "#D63031";

            public static string LogColor = "#88C3EA";

            public static string Warning = "#E4982F";

        }

        /// <summary>
        /// Required tag list
        /// </summary>
        public static List<string> RequiredTags = new List<string>
        {
            "html","head","title","body"
        };
    }
}

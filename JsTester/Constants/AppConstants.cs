﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsTester.Constants
{
    internal class AppConstants
    {
        /// <summary>
        /// A default font name
        /// </summary>
        public static String DefaultUIFontName = "Segoe UI";

        /// <summary>
        /// A default editor font name
        /// </summary>
        public static String DefaultEditorFontName = "Consolas";

        /// <summary>
        /// App GUI files path
        /// </summary>
        public static String GUIPath = "GUI";

        /// <summary>
        /// CSV html tags path
        /// </summary>
        public static String CsvHtmlPath = GUIPath + "/html_tags.csv";

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace JsTester.Core
{
    internal class WebPageValidator
    {
        public string htmlText;
        public WebPageValidator()
        {

        }

        /// <summary>
        /// Page validator
        /// </summary>
        /// <param name="strStart">Start tag</param>
        /// <param name="strEnd">End tag</param>
        /// <returns>End tag index</returns>
        public int ValidatePage(string strStart, string strEnd)
        {
            strStart = "<head>";
            strEnd = "</head>";
            if (string.IsNullOrEmpty(htmlText)) return 0;
            if (htmlText.Contains(strStart) && htmlText.Contains(strEnd))
            {
                int Start, End;
                Start = htmlText.IndexOf(strStart, 0) + strStart.Length;
                End = htmlText.IndexOf(strEnd, Start);
                return End-1;
            } else return 0;
        }
    }
}

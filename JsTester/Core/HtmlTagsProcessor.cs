﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsTester.Constants;
using System.IO;

namespace JsTester.Core
{
    class HtmlTagsProcessor
    {
        /// <summary>
        /// Dictionary (HTML_tag, description)
        /// </summary>
        public Dictionary<string, string> HtmlTags;

        /// <summary>
        /// Joined html tags string
        /// </summary>
        public string HtmlTagsAutoC { get; set; }

        public HtmlTagsProcessor()
        {
            Setup();
        }

        /// <summary>
        /// Setup html tags
        /// </summary>
        void Setup()
        {
            StreamReader sr = new StreamReader(AppConstants.CsvHtmlPath);
            var content = sr.ReadToEnd();
            sr.Close();

            HtmlTags = ParseCsvTags(content);
            HtmlTagsAutoC = GenerateTagsString();
        }

        /// <summary>
        /// Generates full string from tags list
        /// </summary>
        /// <returns>Full HTML tags string, separated " "</returns>
        private string GenerateTagsString()
        {
            return String.Join(" ", HtmlTags.Keys);
        }

        Dictionary<string, string> ParseCsvTags(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }
            Dictionary<string, string> data = new Dictionary<string, string>();
            var splitted = content.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();
            splitted.ForEach(x => {
                if (!string.IsNullOrEmpty(x))
                {
                    var tag = x.Split(';');
                    data.Add(tag[0], tag[1]);
                }
            });
            data.Remove("Tag");
            return data;
        }
    }
}

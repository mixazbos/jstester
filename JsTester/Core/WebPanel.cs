﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Label = System.Windows.Forms.Label;

namespace JsTester.Core
{
    internal class WebPanel
    {
        /// <summary>
        /// Always update a web page
        /// </summary>
        /// <param name="codeBox">Code rich textbox</param>
        /// <param name="browser">Web browser</param>
        public void UpdatePage(ScintillaNET.Scintilla codeBox, WebBrowser browser)
        {
            browser.DocumentText = codeBox.Text;
        }

        /// <summary>
        /// Add a HTML tag into codebox
        /// </summary>
        /// <param name="tag">HTML tag</param>
        /// <param name="codeBox">Codebox</param>
        public void AddTag(string tag, ScintillaNET.Scintilla codeBox)
        {
            WebPageValidator validator = new WebPageValidator() {htmlText = codeBox.Text};
            var startIndex = validator.ValidatePage("", "");
            if (!string.IsNullOrEmpty(tag))
            {
                if (startIndex != 0)
                {
                    codeBox.InsertText(startIndex, tag + Environment.NewLine);
                }
                else
                {
                    codeBox.InsertText(0, tag + Environment.NewLine);
                }

            }
            else MessageBox.Show("Error adding HTML tag");
        }
    }
}

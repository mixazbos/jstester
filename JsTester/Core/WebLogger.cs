﻿using JsTester.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JsTester.Core
{
    class WebLogger
    {
        public static WebBrowser Console;

        // Define error types
        LogTemplate ErrorMess = new LogTemplate(WebConstants.LogTypes.Error.ToString());
        LogTemplate LogMess = new LogTemplate(WebConstants.LogTypes.Log.ToString());
        LogTemplate WarningMess = new LogTemplate(WebConstants.LogTypes.Warning.ToString());

        public WebLogger()
        {

        }

        public void Init()
        {
            Form f1 = Application.OpenForms["Form1"];
            var console = (WebBrowser)f1.Controls["ui_webconsole"];
            Console = console;
        }

        /// <summary>
        /// Simple log message
        /// </summary>
        /// <param name="message">Display message</param>
        public void Log(string message)
        {
            var messageHTML = LogMess.Template;
            var logDiv = $"<div class='consoleErr'><span class='icon log-icon'>?</span>" +
                        $"<span style='padding-left: 20px;'>{message}</span></div>";

            Console.DocumentText = messageHTML + logDiv;
        }

        /// <summary>
        /// Error message
        /// </summary>
        /// <param name="message">Display message</param>
        public void Error(string message)
        {
            var messageHTML = ErrorMess.Template;
            var logDiv = $"<div class='consoleErr'><span class='icon err-icon'>X</span>" +
                        $"<span style='padding-left: 20px;'>{message}</span></div>";

            Console.DocumentText = messageHTML + logDiv;
        }

        /// <summary>
        /// Warning message
        /// </summary>
        /// <param name="message">Display message</param>
        public void Warning(string message)
        {
            var messageHTML = WarningMess.Template;
            var logDiv = $"<div class='consoleErr'><span class='icon warn-icon'>!</span>" +
                        $"<span style='padding-left: 20px;'>{message}</span></div>";

            Console.DocumentText = messageHTML + logDiv;
        }

        /// <summary>
        /// Clear console
        /// </summary>
        public void ClearConsole()
        {
            Console.DocumentText = string.Empty;
        }
    }
}

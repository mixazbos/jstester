﻿using JsTester.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsTester.Core
{
    internal class LogTemplate
    {
        /// <summary>
        /// Templates path
        /// </summary>
        readonly string path = @"Templates";

        /// <summary>
        /// HTML template code
        /// </summary>
        public string Template;

        public LogTemplate(string type)
        {
            string color = string.Empty;
            if (!string.IsNullOrEmpty(type))
            {
                color = SetBackground(type);
            }
            Template = GetTemplate(path) + @"<style>.consoleErr{background - color:" + color +";}</style>";
        }

        /// <summary>
        /// Get log message template
        /// </summary>
        /// <param name="type">Message type</param>
        /// <returns>HTML message</returns>
        string GetTemplate(string type)
        {
            if (!string.IsNullOrEmpty(type))
            {
                StreamReader sr = new StreamReader(path + @"\log_message.mhtml");
                string text = sr.ReadToEnd();
                sr.Close();
                return text;
            } else return string.Empty;
        }

        /// <summary>
        /// Set backgound color
        /// </summary>
        /// <param name="type">Log type</param>
        /// <returns>Hex color</returns>
        string SetBackground(string type)
        {
            switch (type)
            {
                case "Error":
                    return WebConstants.Colors.ErrorColor;

                case "Log":
                    return WebConstants.Colors.LogColor;

                default: return string.Empty;
                case "Warning":
                    return WebConstants.Colors.Warning;
            }
        }
    }
}

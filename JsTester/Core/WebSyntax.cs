﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using JsTester.GUI;
using ScintillaNET;

namespace JsTester.Core
{
    class WebSyntax
    {
        /// <summary>
        /// Scintilla control
        /// </summary>
        private Scintilla ScintillaCtrl;

        /// <summary>
        /// Hightlight a HTML syntax
        /// </summary>
        /// <param name="scintilla">Scintilla code box</param>
        public void Hightlight(Scintilla scintilla)
        {
            ScintillaCtrl = scintilla;
            scintilla.StyleResetDefault();
            scintilla.Styles[Style.Default].Font = HasFont("Cascadia Code");
            scintilla.Styles[Style.Default].Size = 15;
            scintilla.Styles[Style.Default].ForeColor = Color.FromArgb(255, 234, 188, 96);
            scintilla.Styles[Style.Default].BackColor = Color.FromArgb(255, 1, 32, 48);
            scintilla.StyleClearAll();

            scintilla.Styles[Style.Html.Default].ForeColor = Color.FromArgb(255, 154, 235, 163);
            scintilla.Styles[Style.Html.Attribute].ForeColor = Color.White;
            scintilla.Styles[Style.Html.Number].ForeColor = Color.Green;
            scintilla.Styles[Style.Html.Comment].ForeColor = Color.FromArgb(255, 96, 234, 96);
            scintilla.Styles[Style.Html.Script].ForeColor = Color.FromArgb(255, 96, 119, 234);
            scintilla.Styles[Style.Html.Asp].ForeColor = Color.FromArgb(255, 96, 182, 236);
            scintilla.Lexer = ScintillaNET.Lexer.Html;
        }

        /// <summary>
        /// Get installed font
        /// </summary>
        /// <param name="fontName">Font name</param>
        /// <returns>Font name or default name</returns>
        private string HasFont(string fontName)
        {
            FontProvider provider = new FontProvider();
            if (provider.HasInstalledFont(fontName))
            {
                return fontName;
            }

            return Constants.AppConstants.DefaultEditorFontName;
        }

        /// <summary>
        /// Check the syntax
        /// </summary>
        public void CheckHTML(WebLogger logger, HtmlTagsProcessor processor)
        {
            var content = string.IsNullOrEmpty(ScintillaCtrl.Text) ? string.Empty : ScintillaCtrl.Text;
            if (string.IsNullOrEmpty(content)) return;

            GetMissingTags(logger, content);
            GetUnclosedTags(logger, content, processor);
        }

        /// <summary>
        /// Get missing tags
        /// </summary>
        /// <param name="logger">Web console</param>
        /// <param name="content">HTML text</param>
        private static void GetMissingTags(WebLogger logger, string content)
        {
            List<string> missingTags = new List<string>();
            Constants.WebConstants.RequiredTags.ForEach(tag =>
            {
                if (!content.Contains($"<{tag}>"))
                {
                    missingTags.Add(tag);
                }
            });
            if (missingTags.Count > 0)
            {
                var tags = string.Join(", ", missingTags);
                logger.Warning($"Tags '{tags}' not found");
                return;
            }
        }

        /// <summary>
        /// Check closed main tags
        /// </summary>
        /// <returns>Not closed tags</returns>
        public void GetUnclosedTags(WebLogger logger, string content, HtmlTagsProcessor processor)
        {
            /*if (string.IsNullOrEmpty(content)) return;

            List<string> unclosedTags = new List<string>();
            List<string> closedTags = new List<string>();
            processor.HtmlTags.Remove("<!--...-->");
            processor.HtmlTags.Remove("<!DOCTYPE> ");
            foreach (var tag in processor.HtmlTags.Keys)
            {
                if (content.Contains($"<{tag}>"))
                {
                    unclosedTags.Add(tag);
                }

                if (content.Contains($"</{tag}>"))
                {
                    closedTags.Add(tag);
                }
            }
            if (unclosedTags.Count > 0) return string.Join(", ", unclosedTags);
            return string.Empty;*/
        }

    }
}

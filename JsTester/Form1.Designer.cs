﻿namespace JsTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ui_browser = new System.Windows.Forms.WebBrowser();
            this.ui_run = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ui_codebox = new ScintillaNET.Scintilla();
            this.ui_mode = new System.Windows.Forms.ComboBox();
            this.ui_status = new System.Windows.Forms.Label();
            this.ui_title = new System.Windows.Forms.Label();
            this.ui_webconsole = new System.Windows.Forms.WebBrowser();
            this.showSettings = new System.Windows.Forms.PictureBox();
            this.resetPage = new System.Windows.Forms.PictureBox();
            this.saveFile = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resetPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveFile)).BeginInit();
            this.SuspendLayout();
            // 
            // ui_browser
            // 
            this.ui_browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ui_browser.Location = new System.Drawing.Point(3, 3);
            this.ui_browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.ui_browser.Name = "ui_browser";
            this.ui_browser.ScriptErrorsSuppressed = true;
            this.ui_browser.Size = new System.Drawing.Size(378, 388);
            this.ui_browser.TabIndex = 0;
            this.ui_browser.Url = new System.Uri("", System.UriKind.Relative);
            this.ui_browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.ui_browser_DocumentCompleted);
            this.ui_browser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.ui_browser_Navigated);
            // 
            // ui_run
            // 
            this.ui_run.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ui_run.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ui_run.Location = new System.Drawing.Point(723, 7);
            this.ui_run.Name = "ui_run";
            this.ui_run.Size = new System.Drawing.Size(105, 29);
            this.ui_run.TabIndex = 2;
            this.ui_run.Text = "▶ [R] Run";
            this.ui_run.UseVisualStyleBackColor = true;
            this.ui_run.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53F));
            this.tableLayoutPanel1.Controls.Add(this.ui_codebox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ui_browser, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 394F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(819, 394);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // ui_codebox
            // 
            this.ui_codebox.AnnotationVisible = ScintillaNET.Annotation.Boxed;
            this.ui_codebox.AutoCMaxHeight = 9;
            this.ui_codebox.BiDirectionality = ScintillaNET.BiDirectionalDisplayType.Disabled;
            this.ui_codebox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ui_codebox.BufferedDraw = false;
            this.ui_codebox.CaretLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(82)))), ((int)(((byte)(122)))));
            this.ui_codebox.CaretLineVisible = true;
            this.ui_codebox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ui_codebox.Font = new System.Drawing.Font("Cascadia Code", 14.25F);
            this.ui_codebox.LexerName = null;
            this.ui_codebox.Location = new System.Drawing.Point(387, 3);
            this.ui_codebox.Name = "ui_codebox";
            this.ui_codebox.ScrollWidth = 289;
            this.ui_codebox.Size = new System.Drawing.Size(429, 388);
            this.ui_codebox.TabIndents = true;
            this.ui_codebox.TabIndex = 8;
            this.ui_codebox.Technology = ScintillaNET.Technology.DirectWrite;
            this.ui_codebox.Text = "<!DOCTYPE html>\r\n<html>\r\n <head>\r\n  <meta charset=\"utf-8\" />\r\n  <title>Stand</tit" +
    "le>\r\n </head>\r\n <body>\r\n  <h1>Тестовый стенд</h1>\r\n\r\n </body>\r\n</html>";
            this.ui_codebox.UseRightToLeftReadingLayout = false;
            this.ui_codebox.WrapMode = ScintillaNET.WrapMode.None;
            this.ui_codebox.TextChanged += new System.EventHandler(this.ui_codebox_TextChanged);
            // 
            // ui_mode
            // 
            this.ui_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ui_mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ui_mode.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ui_mode.FormattingEnabled = true;
            this.ui_mode.Items.AddRange(new object[] {
            "[h] Html + JS",
            "[J] JS only"});
            this.ui_mode.Location = new System.Drawing.Point(554, 7);
            this.ui_mode.Name = "ui_mode";
            this.ui_mode.Size = new System.Drawing.Size(163, 29);
            this.ui_mode.TabIndex = 4;
            // 
            // ui_status
            // 
            this.ui_status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ui_status.AutoSize = true;
            this.ui_status.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ui_status.ForeColor = System.Drawing.Color.White;
            this.ui_status.Location = new System.Drawing.Point(12, 540);
            this.ui_status.Name = "ui_status";
            this.ui_status.Size = new System.Drawing.Size(38, 15);
            this.ui_status.TabIndex = 5;
            this.ui_status.Text = "label1";
            // 
            // ui_title
            // 
            this.ui_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ui_title.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ui_title.ForeColor = System.Drawing.Color.White;
            this.ui_title.Location = new System.Drawing.Point(8, 11);
            this.ui_title.Name = "ui_title";
            this.ui_title.Size = new System.Drawing.Size(385, 21);
            this.ui_title.TabIndex = 6;
            this.ui_title.Text = "Page Title";
            // 
            // ui_webconsole
            // 
            this.ui_webconsole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ui_webconsole.Location = new System.Drawing.Point(15, 440);
            this.ui_webconsole.MinimumSize = new System.Drawing.Size(20, 20);
            this.ui_webconsole.Name = "ui_webconsole";
            this.ui_webconsole.ScrollBarsEnabled = false;
            this.ui_webconsole.Size = new System.Drawing.Size(816, 97);
            this.ui_webconsole.TabIndex = 7;
            // 
            // showSettings
            // 
            this.showSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showSettings.Image = ((System.Drawing.Image)(resources.GetObject("showSettings.Image")));
            this.showSettings.Location = new System.Drawing.Point(518, 6);
            this.showSettings.Name = "showSettings";
            this.showSettings.Size = new System.Drawing.Size(30, 30);
            this.showSettings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showSettings.TabIndex = 8;
            this.showSettings.TabStop = false;
            this.showSettings.Click += new System.EventHandler(this.pictureBox1_Click);
            this.showSettings.MouseLeave += new System.EventHandler(this.Gui_mouse_leave);
            this.showSettings.MouseHover += new System.EventHandler(this.Gui_mouse_hover);
            // 
            // resetPage
            // 
            this.resetPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.resetPage.Image = ((System.Drawing.Image)(resources.GetObject("resetPage.Image")));
            this.resetPage.Location = new System.Drawing.Point(482, 6);
            this.resetPage.Name = "resetPage";
            this.resetPage.Size = new System.Drawing.Size(30, 30);
            this.resetPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.resetPage.TabIndex = 9;
            this.resetPage.TabStop = false;
            this.resetPage.Click += new System.EventHandler(this.PictureBox2_Click);
            this.resetPage.MouseLeave += new System.EventHandler(this.Gui_mouse_leave);
            this.resetPage.MouseHover += new System.EventHandler(this.Gui_mouse_hover);
            // 
            // saveFile
            // 
            this.saveFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveFile.Image = ((System.Drawing.Image)(resources.GetObject("saveFile.Image")));
            this.saveFile.Location = new System.Drawing.Point(446, 6);
            this.saveFile.Name = "saveFile";
            this.saveFile.Size = new System.Drawing.Size(30, 30);
            this.saveFile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.saveFile.TabIndex = 10;
            this.saveFile.TabStop = false;
            this.saveFile.MouseLeave += new System.EventHandler(this.Gui_mouse_leave);
            this.saveFile.MouseHover += new System.EventHandler(this.Gui_mouse_hover);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(64)))), ((int)(((byte)(89)))));
            this.ClientSize = new System.Drawing.Size(843, 564);
            this.Controls.Add(this.saveFile);
            this.Controls.Add(this.resetPage);
            this.Controls.Add(this.showSettings);
            this.Controls.Add(this.ui_webconsole);
            this.Controls.Add(this.ui_title);
            this.Controls.Add(this.ui_status);
            this.Controls.Add(this.ui_mode);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ui_run);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "JS Tester";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resetPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser ui_browser;
        private System.Windows.Forms.Button ui_run;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox ui_mode;
        private System.Windows.Forms.Label ui_status;
        private System.Windows.Forms.Label ui_title;
        private System.Windows.Forms.WebBrowser ui_webconsole;
        private ScintillaNET.Scintilla ui_codebox;
        private System.Windows.Forms.PictureBox showSettings;
        private System.Windows.Forms.PictureBox resetPage;
        private System.Windows.Forms.PictureBox saveFile;
    }
}


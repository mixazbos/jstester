﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsTester.Models
{
    class Settings
    {
        /// <summary>
        /// Default mode in App
        /// </summary>
        public int defaultMode { get; set; }

        /// <summary>
        /// Included scripts into page
        /// </summary>
        public List<string> includedScripts { get; set; }

        /// <summary>
        /// Available scripts file
        /// </summary>
        public string availableScriptsFile { get; set; }
    }
}

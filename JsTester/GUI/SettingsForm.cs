﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Forms;
using JsTester.Constants;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;
using JsTester.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace JsTester.Core
{
    class SettingsForm
    {
        /// <summary>
        /// Settings form reference
        /// </summary>
        public Form MainForm;

        /// <summary>
        /// Global settings path
        /// </summary>
        string path = @"settings.json";

        /// <summary>
        /// A web panel reference
        /// </summary>
        WebPanel webPanel = new WebPanel();

        /// <summary>
        /// Web console
        /// </summary>
        WebLogger WebConsole = new WebLogger();

        /// <summary>
        /// Settings form constructor
        /// </summary>
        public SettingsForm()
        {
            // Settimgs form
            Form settingsForm = new Form();

            settingsForm.Text = "Settings";
            settingsForm.Size = new Size(300, 600);
            settingsForm.BackColor = Color.FromArgb(255, 2, 64, 89);
            settingsForm.Icon = new Icon(@"gui_settings.ico");
            settingsForm.MaximizeBox = false;
            settingsForm.FormBorderStyle = FormBorderStyle.FixedSingle;

            LoadJson(settingsForm);
            MainForm = settingsForm;
            settingsForm.Show();
        }

        /// <summary>
        /// Font library
        /// </summary>
        Dictionary<string, Font> fonts = new Dictionary<string, Font>()
        {
            {"1", new Font("Segoe UI", 14)},
            {"2", new Font("Consolas", 14)}
        };

        /// <summary>
        /// Load a JSON from file
        /// </summary>
        /// <param name="form">Settings form</param>
        void LoadJson(Form form)
        {
            var content = File.ReadAllText(path);
            var settingsObj = SetupSettings(content);

            SetupGUI(form, settingsObj);
        }

        /// <summary>
        /// Deserialize JSON
        /// </summary>
        /// <param name="json">JSON content</param>
        /// <returns>Settings model</returns>
        Settings SetupSettings(string json)
        {
            Settings settings = JsonConvert.DeserializeObject<Settings>(json);
            return settings;
        }

        /// <summary>
        /// Setup a GUI of the form
        /// </summary>
        /// <param name="form">Seettings form</param>
        /// <param name="settings">Settings model</param>
        void SetupGUI(Form form, Settings settings)
        {
            // Get available scripts
            List<string> includesScripts = LoadIncludes(settings.availableScriptsFile);

            // List of Controlls
            List<System.Windows.Forms.Control> controls = new List<System.Windows.Forms.Control>();

            // Get default font
            var font = fonts.FirstOrDefault(x => x.Value.FontFamily.Name == AppConstants.DefaultUIFontName).Value;

            // Title
            System.Windows.Forms.Label title = new System.Windows.Forms.Label();
            title.Text = "App settings";
            title.Location = new Point(10, 10);
            title.ForeColor = Color.White;
            title.Font = font;
            title.AutoSize = true;
            controls.Add(title);

            // Label included scripts
            System.Windows.Forms.Label titleScripts = new System.Windows.Forms.Label();
            titleScripts.Text = "Include scripts";
            titleScripts.Location = new Point(10, 40);
            titleScripts.ForeColor = Color.White;
            titleScripts.Font = new Font("Segoe UI", 9);
            titleScripts.AutoSize = true;
            controls.Add(titleScripts);

            // Select scripts
            System.Windows.Forms.CheckedListBox includes = new System.Windows.Forms.CheckedListBox();
            includes.Location = new Point(10, 75);
            includes.Font = new Font("Segoe UI", 9);
            includesScripts.ForEach(x =>
            {
                var cleared = x.Replace("\r\n", string.Empty);
                includes.Items.Add(cleared);
            });
            includes.Size = new Size(263, 150);
            controls.Add(includes);

            // Save button
            System.Windows.Forms.Button saveButton = new System.Windows.Forms.Button();
            saveButton.Text = "Save";
            saveButton.Size = new Size(140, 30);
            saveButton.Location = new Point(145, 526);
            saveButton.ForeColor = Color.White;
            saveButton.Font = font;
            saveButton.Click += delegate (Object o, EventArgs e)
            {
                Settings newSettings = new Settings();
                newSettings.availableScriptsFile = settings.availableScriptsFile;
                newSettings.includedScripts = new List<string>();
                newSettings.defaultMode = settings.defaultMode;

                for (int i = 0; i <= (includes.Items.Count - 1); i++)
                {
                    if (includes.GetItemChecked(i))
                    {
                        newSettings.includedScripts.Add(includes.Items[i].ToString());
                    }
                }

                SaveSettings(newSettings);
                UpdateCodeBox(newSettings);
                form.Close();
            };
            controls.Add(saveButton);

            // Cancel button
            System.Windows.Forms.Button cancelButton = new System.Windows.Forms.Button();
            cancelButton.Text = "Cancel";
            cancelButton.Size = new Size(140, 30);
            cancelButton.Location = new Point(0, 526);
            cancelButton.ForeColor = Color.White;
            cancelButton.Font = font;
            cancelButton.Click += delegate (Object o, EventArgs e)
            {
                MainForm.Close();
            };
            controls.Add(cancelButton);


            // Adding a controlls into settings form
            controls.ForEach(ctrl =>
            {
                form.Controls.Add(ctrl);
            });
        }

        /// <summary>
        /// Update the codebox
        /// </summary>
        /// <param name="settings">Settings</param>
        void UpdateCodeBox(Settings settings)
        {
            Form f1 = Application.OpenForms["Form1"];
            var codeBox = (ScintillaNET.Scintilla)f1.Controls["tableLayoutPanel1"].Controls["ui_codebox"];

            // TODO code snippet
            settings.includedScripts.ForEach(item => {
                webPanel.AddTag($"<script src='{item}'></script>", codeBox);
            });
            WebConsole.Log("Script added");
        }

        /// <summary>
        /// Save the settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SaveSettings(Settings newSettings)
        {
            if (newSettings != null)
            {
                var jsonSettings = JsonConvert.SerializeObject(newSettings);
                StreamWriter sw = new StreamWriter(path);
                sw.Write(jsonSettings);
                sw.Close();
            }
            else MessageBox.Show("Settings save error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Load includes scripts from file
        /// </summary>
        /// <param name="path">Includes file path</param>
        /// <returns>List of scripts</returns>
        List<string> LoadIncludes(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                var scripts = File.ReadAllText(@path);
                return scripts.Split(';').ToList();
            }
            else return null;
        }
    }
}

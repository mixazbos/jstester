﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Text;

namespace JsTester.GUI
{
    public class FontProvider
    {
        /// <summary>
        /// Installed fonts list
        /// </summary>
        readonly List<string> InstalledFonts = new List<string>();

        public FontProvider()
        {
            GetAllInstalledFonts();
        }

        /// <summary>
        /// Gets all installed fonts from Windows
        /// </summary>
        private void GetAllInstalledFonts()
        {
            using (InstalledFontCollection col = new InstalledFontCollection())
            {
                foreach (FontFamily family in col.Families)
                {
                    InstalledFonts.Add(family.Name);
                }
            }
        }

        /// <summary>
        /// Check has specified font installed
        /// </summary>
        /// <param name="fontName">Font name</param>
        /// <returns>Font installed</returns>
        public bool HasInstalledFont(string fontName)
        {
            if (string.IsNullOrEmpty(fontName)) throw new Exception("Font name is null or empty");
            return InstalledFonts.Contains(fontName);
        }
    }
}
